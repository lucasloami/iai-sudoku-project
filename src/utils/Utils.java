package utils;

import java.util.Random;

public abstract class Utils {
	
	public Utils () {}
	
	public Utils(int p, int q, int n){}
	
	/**
	 * Generate a random number in a predefined range
	 * 
	 * @param min
	 * @param max
	 * @return randNum
	 */
	public static int randInt(int min, int max) {
		Random rand = new Random();
		int randNum = rand.nextInt((max - min) +1) + min;
		return randNum;
		
	}
	
	/**
	 * Generate a random position in a predefined matrix
	 * 
	 * @param n
	 * @return matrix position
	 */
	public static int[] randMatrixPosition(int n) {
		int i = randInt(0, n-1);
		int j = randInt(0, n-1);
		int[] k = new int[]{i, j}; 
		
		return k;
	}
	
	/**
	 * 
	 * Count number of positions avalailable in a predefined matrix
	 * Positions available have value = -1
	 * 
	 * @param matrix
	 * @param n
	 * @return number of positions available
	 */
	public static int countIf(int[][] matrix, int n) {
		int i = 0;
		
		for (int j = 0; j < matrix.length; j++) {
			for (int j2 = 0; j2 < matrix[0].length; j2++) {
				if (matrix[j][j2] == n)
					i++;
			}
		}
		
		return i;
		
	}
	
	public static int countIf(int[][][] matrix, int n) {
		int i = 0;
		
		for (int j = 0; j < matrix.length; j++) {
			for (int j2 = 0; j2 < matrix[0].length; j2++) {
				if (matrix[j][j2][0] == n)
					i++;
			}
		}
		
		return i;
		
	}
	
	public static int[][] fillArray(int[][] arrayToFill, int[][] arrayToBeFilled) {
		//int[][] finalArray = null;
		//TODO implementation
		
		return arrayToFill;
	}
	
	

}

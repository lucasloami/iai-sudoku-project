package userInterface;

import java.awt.BorderLayout;
import java.awt.Point;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * Panel responsible to group the user interface panels.
 * 
 * @author rafaelsimao
 *
 */
public class Board extends JPanel{
	
	/**
	 * serialVersionUID = 8770815885155964332L;
	 */
	private static final long serialVersionUID = 8770815885155964332L;
	
	private MenuPanel menuPanel;
	private SudokuPanel sudokuPanel;
	
	public Board(){
		setLayout(new BorderLayout(0, 0));
		
		//MENU PANEL
		menuPanel = new MenuPanel(this);
		add(menuPanel, BorderLayout.WEST);
		
		//SUDOKU PANEL
		sudokuPanel = new SudokuPanel();
		add(sudokuPanel, BorderLayout.CENTER);
	}
	
	/**
	 * Updates board contents
	 */
	public void updateBoard(){
		if(sudokuPanel != null)
			sudokuPanel.updateSudoku();

		if (SwingUtilities.getWindowAncestor(this).getSize().width < 800)
			SwingUtilities.getWindowAncestor(this).setSize(900,600);
		SwingUtilities.getWindowAncestor(this).setLocation(new Point(0,0));
	}

	/**
	 * @return the menuPanel
	 */
	public MenuPanel getMenuPanel() {
		return menuPanel;
	}

	/**
	 * @return the sudokuPanel
	 */
	public SudokuPanel getSudokuPanel() {
		return sudokuPanel;
	}
	
}

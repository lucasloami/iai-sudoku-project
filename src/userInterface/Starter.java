package userInterface;

/**
 * Starts the application.
 * 
 * @author rafaelsimao
 *
 * Verificar Input
 * - Tamanho do sudoku n>=9
 * - Ver se p*q=n
 * - SOLVER : Opces de ligar e desligar heuristicas
 * 
 * 
 */
public class Starter {
	
	public static void main(String[] args){
		Board board = new Board();
		Window.GetInstance().change(board);
	}
}

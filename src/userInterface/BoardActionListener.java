package userInterface;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.JOptionPane;

import system.Facade;
import system.Sudoku;
import system.SudokuHandler;

/**
 * Make the button interactions.
 * @author rafaelsimao
 *
 */
public class BoardActionListener implements ActionListener{

	static final String BOARD_ACTION_NEW_SUDOKU = "1";
	static final String BOARD_ACTION_SOLVE 		= "2";
	static final String BOARD_ACTION_RANDOM 	= "3";
	static final String BOARD_ACTION_GENERATE_CHART_DATA = "4";
	
	static final String ERROR_INVALID_ENTRIES = "P,Q or N are invalid, please reenter with a valid information!";
	static final String ERROR_EMPTY_INPUT = "Please enter a valid input file!";
	static final String ERROR_GENERATOR = "The generator couldn't generate one sudoku with the specified m, please try again!";
	
	private Board board;
	
	public BoardActionListener(Board board) {
		this.board = board;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		switch(e.getActionCommand()){
			/*case BOARD_ACTION_NEW_SUDOKU:
				int p,q,n;
				p= board.getMenuPanel().getP();
				q= board.getMenuPanel().getQ();
				n= board.getMenuPanel().getN();
				if((p<3)||(q<3)||(n<9)||(p*q!=n)){
					System.out.println(""+p+" "+q+" "+n);
					JOptionPane.showMessageDialog(board, ERROR_INVALID_ENTRIES, null, JOptionPane.ERROR_MESSAGE);
				}else{
					Facade.getInstance().createNewSudoku(p, q, n);
					board.updateBoard();
				}
				break;*/
			case BOARD_ACTION_SOLVE:
				if(!isInputEmpty(board.getMenuPanel().getfileInput())){
					Facade.getInstance().startTimeTotal();
					if(readInput(board.getMenuPanel().getfileInput())){
						Facade.getInstance().startTimeSearch();
						Facade.getInstance().solveSudoku(takeCSPs());
						Facade.getInstance().stopTimeSearch();
						board.updateBoard();
					}
					Facade.getInstance().stopTimeTotal();
					//System.out.println(Facade.getInstance().getTimeSearch());
					//System.out.println(Facade.getInstance().getTimeTotal());
				}
				break;
			case BOARD_ACTION_RANDOM:
				if(!isInputEmpty(board.getMenuPanel().getfileInput())){
					if(readGeneratorInput(board.getMenuPanel().getfileInput())){
						if(Facade.getInstance().sudokuFillRandom()){
							writeOutput(board.getMenuPanel().getfileOutput(), Facade.getInstance().getSudoku());
							board.updateBoard();
						}else
							JOptionPane.showMessageDialog(board, ERROR_GENERATOR, null, JOptionPane.ERROR_MESSAGE);
					}
				}
				break;
        	case BOARD_ACTION_GENERATE_CHART_DATA:
                Sudoku sudoku = Facade.getInstance().createNewSudoku(3,3,9);
                long timeTotal = 0;
                long timeSearch = 0;
                int notSolvedCounter = 0;
                boolean isSolved;
                int j;
                for (int i = 1; i < 41; i++) {

                	timeTotal = 0;
                	timeSearch = 0;
                	notSolvedCounter = 0;
                	j = 0;
                	sudoku.setM(i);
                	while (j < 56) {
                		Facade.getInstance().sudokuFillRandom();
                		Facade.getInstance().startTimeTotal();
                		Facade.getInstance().startTimeSearch();
                		Facade.getInstance().solveSudoku(takeCSPs());
                		isSolved = Facade.getInstance().didSolve();
                		Facade.getInstance().stopTimeSearch();
                		Facade.getInstance().stopTimeTotal();
                		if (isSolved && !Facade.getInstance().didTimeOut()) {
                			timeTotal += Facade.getInstance().getTimeTotal();
                			//System.out.println(timeTotal);
                			timeSearch += Facade.getInstance().getTimeSearch();
                			j++;
                		}
                		else {
                			
                			notSolvedCounter++;
                			
                		}
                		
                	}
                      
                	//System.out.println(i+"\t"+timeTotal/j+"\t"+timeSearch/j+"\t"+notSolvedCounter);
                	System.out.println(i+"\t"+timeTotal/j+"\t"+notSolvedCounter);
                }
                break;
			default:
				System.out.println("BoardActionListener - default action performed");
				break;
		}
	}

	private int takeCSPs(){
		if(board.getMenuPanel().isWithFowardChecking())
			return SudokuHandler.CSP_FC;
		return SudokuHandler.CSP_NO;
	}
	
	/**
	 * verify if the input file is empty
	 * @param arquivo
	 * @return
	 */
	private boolean isInputEmpty(String arquivo){
		
		File arq;
		arq = new File(arquivo);
		if (!arq.exists()) {
			JOptionPane.showMessageDialog(board, ERROR_EMPTY_INPUT, null, JOptionPane.ERROR_MESSAGE);
			return true;
		}
		
		return false;
	}
	
	/**
	 * Verify if the sudoku entries are correct
	 * @param p
	 * @param q
	 * @param n
	 * @return
	 */
	private boolean isInputsValid(int p,int q,int n) {
		if((n<9)||(p*q!=n)){
			JOptionPane.showMessageDialog(board, ERROR_INVALID_ENTRIES, null, JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}
	
	private boolean isInputsValid(int p,int q,int n, int m) {
		if(!isInputsValid(p, q, n))
			return false;
		else if((m<0)||(m>(n*n-1))){
			JOptionPane.showMessageDialog(board, ERROR_INVALID_ENTRIES, null, JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}
	
	/**
	 * Read n,p,q and the matrix, then returns them in an array of 4
	 * @param arquivo
	 * @return
	 */
	private boolean readInput(String arquivo) {
		FileReader file;
		int n,p,q;
		n = p = q = 0;
		Sudoku sudoku = null;
		try {
			file = new FileReader(arquivo);
			Scanner scan = new Scanner(file);
			n = scan.nextInt();
			p = scan.nextInt();
			q = scan.nextInt();
			if(!isInputsValid(p, q, n))
				return false;
			
			sudoku = Facade.getInstance().createNewSudoku(p,q,n);
			
			for (int i = 0; i < sudoku.getN(); ++i) {
				for (int j = 0; j < sudoku.getN(); j++) {
					sudoku.setSudokuElement(scan.nextInt(), i, j);
				}
			}
			
			scan.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return true;
	}
	
	/**
	 * Read n,p,q and m, then returns them in an array of 4
	 * @param arquivo
	 * @return
	 */
	private boolean readGeneratorInput(String arquivo) {
		FileReader file;
		int n,p,q,m;
		n = p = q = m = 0;

		try {
			file = new FileReader(arquivo);
			Scanner scan = new Scanner(file);
			n = scan.nextInt();
			p = scan.nextInt();
			q = scan.nextInt();
			m = scan.nextInt();
			scan.close();
			if(!isInputsValid(p, q, n, m))
				return false;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		Sudoku sudoku = Facade.getInstance().createNewSudoku(p,q,n);
		sudoku.setM(m);
		
		return true;
	}
	
	/**
	 * Write the output of a sudoku in a file.
	 * @param arquivo
	 * @param sudoku
	 */
	private void writeOutput(String arquivo, Sudoku sudoku) {

		File arq;
		try {
			arq = new File(arquivo);
			if (arq.exists()) {
				arq.delete();
			}
			arq.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		FileWriter fout;
		try {
			fout = new FileWriter(arquivo,true);
			BufferedWriter bw = new BufferedWriter(fout);
			bw.write(sudoku.getN()+" "+sudoku.getP()+" "+sudoku.getQ()+"\n");
			for (int i = 0; i < sudoku.getN(); ++i) {
				for (int j = 0; j < sudoku.getN(); j++) {
					if(j!=(sudoku.getN()-1))
						bw.write(sudoku.getSudokuElement(i, j)+" ");
					else
						bw.write(sudoku.getSudokuElement(i, j)+"\n");
				}
				//bw.write("\n");
			}
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}

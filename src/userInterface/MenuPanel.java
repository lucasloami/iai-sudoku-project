package userInterface;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * Panel responsible for showing the actions menu.
 * 
 * @author rafaelsimao
 *
 */
public class MenuPanel extends JPanel{
	
	/**
	 * serialVersionUID = -7145793777801145727L;
	 */
	private static final long serialVersionUID = -7145793777801145727L;

	private static final int MENU_WIDTH  = 200;
	private static final int MENU_HEIGHT = 800;
	private static final int BT_HEIGHT	 = 50;

	//private static final String BT_NEW_SUDOKU = "New Sudoku";
	private static final String BT_GENERATE_CHART_DATA 	  = "Generate Chart Data";
	private static final String BT_SOLVE 	  = "Solver";
	private static final String BT_RANDOM 	  = "Generator";
	
	/*
	private static final String LABEL_P = "P: ";
	private static final String LABEL_Q = "Q: ";
	private static final String LABEL_N = "N: ";
	
	private static final String DEFAULT_P = "3";
	private static final String DEFAULT_Q = "3";
	private static final String DEFAULT_N = "9";
	*/
	
	//private JButton btNewSudoku, btSolve, btRandom;
	private JButton btSolve, btRandom, btGenerateChartData;
	
	private JPanel panelFileI,panelFileO;
	//private String FilepathI = "",FilepathO = "";
	private JButton btFileChooserI, btFileChooserO;
	private JTextField tfPathI, tfPathO;
	private JFileChooser fChooser;
	
	private JCheckBox cbFowardChecking,cbMRV,cbDegree,cbLCV,cbACP,cbAC;
	private JPanel pnLabels;
	
	//private JLabel labelP, labelQ, labelN;
	//private JTextField tfP, tfQ, tfN;
	
	BoardActionListener boardActionListener;
	
	public MenuPanel(Board board){
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		setPreferredSize(new Dimension(MENU_WIDTH, MENU_HEIGHT));
		setMaximumSize(new Dimension(MENU_WIDTH, MENU_HEIGHT));
		
		boardActionListener = new BoardActionListener(board);
		
		/*Create JLabels and JTextFields: */
		/*
		labelP = new JLabel(LABEL_P);
		tfP = new JTextField(DEFAULT_P);
		add(labelP);
		add(tfP);
		labelQ = new JLabel(LABEL_Q);
		tfQ = new JTextField(DEFAULT_Q);
		add(labelQ);
		add(tfQ);
		labelN = new JLabel(LABEL_N);
		tfN = new JTextField(DEFAULT_N);
		add(labelN);
		add(tfN);
		/*
		/*Initialize the menu buttons: */
		
		initPanelFileI();
		initPanelFileO();
		
		//New Sudoku
		/*
		btNewSudoku = new JButton(BT_NEW_SUDOKU);
		btNewSudoku.setAlignmentX(CENTER_ALIGNMENT);
		btNewSudoku.setPreferredSize(new Dimension(MENU_WIDTH, BT_HEIGHT));
		btNewSudoku.setActionCommand(BoardActionListener.BOARD_ACTION_NEW_SUDOKU);
		btNewSudoku.addActionListener(boardActionListener);
		add(btNewSudoku);
		*/

		//Random Generator
		btRandom = new JButton(BT_RANDOM);
		btRandom.setAlignmentX(CENTER_ALIGNMENT);
		btRandom.setPreferredSize(new Dimension(MENU_WIDTH, BT_HEIGHT));
		btRandom.setActionCommand(BoardActionListener.BOARD_ACTION_RANDOM);
		btRandom.addActionListener(boardActionListener);
		add(btRandom);
		
		//Solve Sudoku
		btSolve = new JButton(BT_SOLVE);
		btSolve.setAlignmentX(CENTER_ALIGNMENT);
		btSolve.setPreferredSize(new Dimension(MENU_WIDTH, BT_HEIGHT));
		btSolve.setActionCommand(BoardActionListener.BOARD_ACTION_SOLVE);
		btSolve.addActionListener(boardActionListener);
		add(btSolve);
		
		initCheckBoxes();
		
		//Generate Chart Data
		btGenerateChartData = new JButton(BT_GENERATE_CHART_DATA);
		btGenerateChartData.setAlignmentX(CENTER_ALIGNMENT);
		btGenerateChartData.setPreferredSize(new Dimension(MENU_WIDTH, BT_HEIGHT));
		btGenerateChartData.setActionCommand(BoardActionListener.BOARD_ACTION_GENERATE_CHART_DATA);
		btGenerateChartData.addActionListener(boardActionListener);
		add(btGenerateChartData);
		
		add(Box.createRigidArea(new Dimension(MENU_WIDTH,MENU_HEIGHT/2)));
	
	}
	
	private void initCheckBoxes(){
		cbFowardChecking = new JCheckBox("Foward Checking");
		cbMRV = new JCheckBox("MRV");
		cbDegree = new JCheckBox("Degree Heuristic");
		cbLCV = new JCheckBox("Least Constraining Value");
		cbACP = new JCheckBox("Arc Consistency Pre-processing");
		cbAC = new JCheckBox("Arc Consistency");
		
		cbDegree.setEnabled(false);
		cbMRV.setEnabled(false);
		cbLCV.setEnabled(false);
		cbACP.setEnabled(false);
		cbAC.setEnabled(false);
		
		pnLabels = new JPanel();
		pnLabels.setLayout(new BoxLayout(pnLabels, BoxLayout.PAGE_AXIS));
		pnLabels.add(cbFowardChecking);
		pnLabels.add(cbMRV);
		pnLabels.add(cbDegree);
		pnLabels.add(cbLCV);
		pnLabels.add(cbACP);
		pnLabels.add(cbAC);
		add(pnLabels);
	}
	
	private void initPanelFileI(){
		//File panel
		panelFileI = new JPanel();
		add(panelFileI);
		panelFileI.setLayout(new BorderLayout(0, 0));
				
		//TEXTFIELD PATH
		tfPathI = new JTextField();
		panelFileI.add(tfPathI, BorderLayout.CENTER);
		tfPathI.setColumns(10);
						
		//File Chooser
		fChooser = new JFileChooser();
		File f = null;
		try {
			f = new File(new File(".").getCanonicalPath());
		} catch (IOException e) {
			e.printStackTrace();
		}
		fChooser.setCurrentDirectory(f);
		btFileChooserI = new JButton("Input...");
		btFileChooserI.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int returnVal = fChooser.showDialog(MenuPanel.this,"");
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					tfPathI.setText(fChooser.getSelectedFile().getPath());
					//FilepathI = fChooser.getSelectedFile().getPath();
				}
			}
		});
		panelFileI.add(btFileChooserI, BorderLayout.EAST);
	}
	
	private void initPanelFileO(){
		//File panel
		panelFileO = new JPanel();
		add(panelFileO);
		panelFileO.setLayout(new BorderLayout(0, 0));
						
		//TEXTFIELD PATH
		tfPathO = new JTextField();
		panelFileO.add(tfPathO, BorderLayout.CENTER);
		tfPathO.setColumns(10);
								
		//File Chooser
		fChooser = new JFileChooser();
		File f = null;
		try {
			f = new File(new File(".").getCanonicalPath());
		} catch (IOException e) {
			e.printStackTrace();
		}
		fChooser.setCurrentDirectory(f);
		btFileChooserO = new JButton("Output...");
		btFileChooserO.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int returnVal = fChooser.showDialog(MenuPanel.this,"");
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					tfPathO.setText(fChooser.getSelectedFile().getPath());
					//FilepathO = fChooser.getSelectedFile().getPath();
				}
			}
		});
		panelFileO.add(btFileChooserO, BorderLayout.EAST);
	}	
	
	/**
	 * @return file input in the text field
	 */
	public String getfileInput(){
		return tfPathI.getText();
	}
	
	/**
	 * @return file output in the text field
	 */
	public String getfileOutput(){
		return tfPathO.getText();
	}
	
	/**
	 * @return if the foward checking is selected to be used
	 */
	public boolean isWithFowardChecking(){
		return cbFowardChecking.isSelected();
	}
	
	/**
	 * @return the tfP
	 */
	/*
	public int getP() {
		//return getTextInt(tfP.getText());
		return 3;
	}*/

	/**
	 * @return the tfQ
	 */
	/*
	public int getQ() {
		//return getTextInt(tfQ.getText());
		return 3;
	}*/

	/**
	 * @return the tfN
	 */
	/*public int getN() {
		//return getTextInt(tfN.getText());
		return 9;
	}*/
	
	
	
	/**
	 * Used to translate the JTextFields string to integer catching the NumberFormatException
	 * @param text
	 * @return parse the text to a integer,
	 * if it throws a NumberFormatException it returns 0
	 */
	/*
	private int getTextInt(String text){
		try {
			return Integer.parseInt(text);
		} catch (NumberFormatException e) {
			return 0;
		}
	}
	*/
}

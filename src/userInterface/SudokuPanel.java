package userInterface;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import system.Facade;
import system.Sudoku;

/**
 * Panel responsible for showing the sudoku grid.
 * 
 * @author rafaelsimao
 *
 */
public class SudokuPanel extends JPanel{

	/**
	 * serialVersionUID = 7514622909565787318L;
	 */
	private static final long serialVersionUID = 7514622909565787318L;

	private static final int MINIMUM_WIDTH = 400;
	private static final int MINIMUM_HEIGHT = 600;
	private static final int MAXIMUM_WIDTH = 1000;
	private static final int MAXIMUM_HEIGHT = 800;
	
	private static final int MIN_TABLE_COLUMN_WIDTH = 26;
	
	private Sudoku sudoku;
	
	private JTable table;
	private JScrollPane scrollPane;
	
	private JLabel lbTimeOut,lbSolved,lbTimeTotal,lbTimeSearch,lbAssignments;
	//private JPanel pnLabels;
	 
	public SudokuPanel(){
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		setPreferredSize(new Dimension(MINIMUM_WIDTH, MINIMUM_HEIGHT));
		setMinimumSize(new Dimension(MINIMUM_WIDTH, MINIMUM_HEIGHT));
		setMaximumSize(new Dimension(MAXIMUM_WIDTH, MAXIMUM_HEIGHT));
		
		sudoku = null;
		
		lbTimeOut = new JLabel();
		lbSolved = new JLabel();
		lbTimeTotal = new JLabel();
		lbTimeSearch = new JLabel();
		lbAssignments = new JLabel();
	}
	
	/**
	 * Updates SudokuPanel state based in the new Sudoku.
	 */
	public void updateSudoku(){
		this.sudoku = Facade.getInstance().getSudoku();
		
		if(table!=null)
			removeAll();
		if(Facade.getInstance().isSudokuValid()){
			table = new JTable(sudoku.getN(),sudoku.getN());
			table.setGridColor(Color.BLACK);
			scrollPane = new JScrollPane( table );
			
			balanceTableSize();
			
			scrollPane.setPreferredSize(new Dimension(MIN_TABLE_COLUMN_WIDTH*sudoku.getN(),
					19*sudoku.getN()));
			
			add(scrollPane);
			
			fillSudoku();
			if(Facade.getInstance().numberOfAssignments()>0)
				fillReportNumbers();
		}

		repaint();
		validate();
		
	}
	
	/**
	 * Balances the table size based in the sudoku dimensions
	 */
	private void balanceTableSize(){
		int i;
		for(i=0;i<table.getColumnCount();i++){
			table.getColumnModel().getColumn(i).setPreferredWidth(MIN_TABLE_COLUMN_WIDTH);
		}
	}
	
	/**
	 * Fills the table with the sudoku content
	 */
	private void fillSudoku()
	{
		for(int i = 0; i<table.getColumnCount(); ++i)
		{
			for(int j = 0; j<table.getRowCount(); ++j){
				if(sudoku.getSudokuElement(i, j) != 0)
					table.setValueAt(sudoku.getSudokuElement(i, j), i, j);
			}
		}
	}
	
	private void fillReportNumbers(){
		lbTimeOut.setText("Timed out: "+Facade.getInstance().didTimeOut());
		lbSolved.setText("Solved: "+Facade.getInstance().didSolve());
		lbAssignments.setText("Assignments: "+Facade.getInstance().numberOfAssignments());
		lbTimeTotal.setText("Time total: "+Facade.getInstance().getTimeTotal());
		lbTimeSearch.setText("Time search: "+Facade.getInstance().getTimeSearch());

		add(lbTimeOut);
		add(lbSolved);
		add(lbAssignments);
		add(lbTimeTotal);
		add(lbTimeSearch);
	}
	
}

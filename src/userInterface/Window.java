package userInterface;

import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * JFrame that creates the window.
 * Through this the program may change the window content.
 * 
 * @author rafaelsimao
 *
 */
public class Window extends JFrame{

        /**
         * serialVersionUID = 96314687473652840L;
         */
        private static final long serialVersionUID = 96314687473652840L;
        
        public static final int INITIAL_WINDOW_WIDTH = 200;
        public static final int INITIAL_WINDOW_HEIGHT = 320;
        
        private static Window window = null;
        
        public static Window GetInstance() {
                if (window == null) {
                        window = new Window();
                }
                return window;
        }
        
        private Window() {
        	//super.setIconImage(new ImageIcon(getClass().getResource("boneco.png")).getImage());
            //add(new Board());
        	setTitle("Sudoku");
        
        	setDefaultCloseOperation(EXIT_ON_CLOSE);
        	addWindowListener(new java.awt.event.WindowAdapter() {
        		public void windowClosing(java.awt.event.WindowEvent e) {
        			System.exit(0);
        		}
        	});
        
        	setSize(INITIAL_WINDOW_WIDTH, INITIAL_WINDOW_HEIGHT);
        	setLocationRelativeTo(null);
        	setVisible(true);
        	setResizable(true);
        	//this.setExtendedState(MAXIMIZED_BOTH);
        }
        
        /**
         * Changes window panel content.
         * @param painel
         */
        public void change(JPanel painel) {
                window.remove(window.getContentPane());
                window.setContentPane(painel);
                window.setVisible(true);
        }
}
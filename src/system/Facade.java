package system;

/**
 * 
 * Summarizes the system functions, and manipulate the sudoku changes (Business layer).
 * 
 * @author rafaelsimao
 *
 */
public class Facade {

	private Sudoku sudoku;
	private SudokuHandler sudokuHandler;
	private boolean didSolve;
	private long time_total;
	private long time_search;
	private static Facade facade = null;
	
	/**
	 * Singleton implementation of the class Facade
	 * @return
	 */
	public static Facade getInstance(){
		if(facade == null)
			facade = new Facade();
		
		return facade;
	}
	
	private Facade(){
		sudoku = null;
		sudokuHandler = null;
		time_total = 0;
		time_search = 0;
		didSolve = false;
	}
	
	/**
	 * Creates one new empty sudoku table with the specified dimensions
	 * @param p
	 * @param q
	 * @param n
	 * @return
	 */
	public Sudoku createNewSudoku(int p, int q, int n){
		sudoku = new Sudoku(p, q, n);
		sudokuHandler = new SudokuHandler(p,q,n);
		
		return sudoku;
	}
	
	/**
	 * Generates the sudoku table filling it with m numbers
	 * @param m
	 */
	public boolean sudokuFillRandom(){
		if(sudoku.getFilledSudokuTable()!=null)
			return true;
		return false;
	}
	
	/**
	 * Solves the sudoku using the sudokuHandler 
	 */
	public void solveSudoku(int csp){
		int[][] sudokuTable = sudokuHandler.solveSudoku(this.sudoku.getSudokuTable(), csp);
		if(sudokuTable!=null){
			this.sudoku.setSudokuTable(sudokuTable);
			didSolve = true;
		}else
			didSolve = false;
		
	}
	
	/**
	 * Verifies if the sudoku is in a valid state
	 * @return true if it is different from null
	 */
	public boolean isSudokuValid(){
		return (sudoku != null);
	}
	
	/**
	 * Get the sudoku object.
	 * @return
	 */
	public Sudoku getSudoku(){
		return sudoku;
	}
	/**
	 * Get TimeTotal
	 * @return
	 */
	public long getTimeTotal(){
		return time_total;
	}
	/**
	 * Get TimeSearch
	 * @return
	 */
	public long getTimeSearch(){
		return time_search;
	}
	/**
	 * start TimeTotal
	 */
	public void startTimeTotal(){
		time_total = System.currentTimeMillis();
	}
	/**
	 * stop TimeTotal
	 */
	public void stopTimeTotal(){
		time_total = System.currentTimeMillis() - time_total;
	}
	/**
	 * start TimeSearch
	 */
	public void startTimeSearch(){
		time_search = System.currentTimeMillis();
	}
	/**
	 * stop TimeSearch
	 */
	public void stopTimeSearch(){
		time_search = System.currentTimeMillis() - time_search;
	}
	/**
	 * Verify if it did timed out
	 * @return false if it didnt and true otherwise
	 */
	public boolean didTimeOut(){
		if(sudokuHandler!=null)
			return sudokuHandler.isTimeOut();
		return false;
	}
	
	/**
	 * @return the didSolve
	 */
	public boolean didSolve() {
		return didSolve;
	}
	
	/**
	 * Get number f assignments done in the sudoku handler while solving a problem
	 * @return
	 */
	public int numberOfAssignments(){
		if(sudokuHandler!=null)
			return sudokuHandler.getAssignments();
		return 0;
	}

}

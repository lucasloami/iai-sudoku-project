package system;

import java.util.Arrays;

import utils.Utils;


/**
 * Object that encapsulates the Sudoku informations and has its representations.
 * 
 * @author rafaelsimao
 *
 */
public class Sudoku {
	
	private int p,q,n,m;
	//private SudokuHandler sudokuHandler;
	private int[][] sudokuTable;
	private int numberBlockColumns; //number of columns of each block
	private int numberBlockRows; //number of columns of each row
	
	/**
	 * Sudoku Constructor, creates one sudoku with the specified dimensions
	 * with all positions filled out with 0's
	 * @param p
	 * @param q
	 * @param n
	 */
	public Sudoku(int p, int q, int n) {
		this.p = p;
		this.q = q;
		this.n = n;
		this.m = 0;
		//sudokuHandler = new SudokuHandler(p, q, n);
		sudokuTable = new int[n][n];
		this.numberBlockColumns = n/p;
		this.numberBlockRows = n/q;
	}
	
	/**
	 * Returns the sudoku table filled with m numbers
	 * @param m
	 * @return
	 */
	public int[][] getFilledSudokuTable() {
		int table[][] = null;
		
		table = this.generateSudokuTable();
		
		return table;
	}
	
	/**
	 * Generates m numbers in the sudoku table
	 * @param m
	 * @return
	 */
	private int[][] generateSudokuTable() {
		int n = this.getN();
		//int loadPercent = Utils.randInt(240, 480);
		//count number of square to be filled
		int numSquares = m;
		int[] position;
		int number;
		
		
		
		int i,squares;//k=0;
		do{
			i=0;
			squares=0;
			for(int ctd=0;ctd<n;++ctd)
				Arrays.fill(sudokuTable[ctd], 0);
			do{
				position = Utils.randMatrixPosition(n);
				number = Utils.randInt(1, 9);
				if(applyAllDiff(number, position, sudokuTable) && (sudokuTable[position[0]][position[1]]==0)){
				sudokuTable[position[0]][position[1]] = number;
				squares++;
				}
				if(squares>=numSquares){
					//System.out.println(i);
					//System.out.println(k);
					//System.out.println(squares);
					return sudokuTable;
				}
				i++;
			}while(i<10000);
		}while(true);

	}

	/**
	 * Applies the constraint AllDiff in the sudokuTable based in the chosen variable
	 * and the number assigned to it.
	 * @param number
	 * @param numberPosition
	 * @param sudokuTable
	 * @return
	 */
	public boolean applyAllDiff(int number, int[] numberPosition, int[][] sudokuTable) {
		//boolean flag = true;
		
		//check AllDiff - row
		for (int i = 0; i < n; i++) {
			if ((i != numberPosition[1]) && (number == sudokuTable[numberPosition[0]][i])) {
				return false;
			}
		}
		
		//check AllDiff - column
		for (int i = 0; i < n; i++) {
			if ((i != numberPosition[0]) && (number == sudokuTable[i][numberPosition[1]])) {
				return false;
			}
		}
		//Block Checking
		int blockX = (int)(Math.floor(numberPosition[0]/numberBlockRows)*numberBlockRows);
		int blockY = (int)(Math.floor(numberPosition[1]/numberBlockColumns)*numberBlockColumns);
		for (int i = blockX; i < blockX + numberBlockColumns; i++) {
			for (int j = blockY; j < blockY + numberBlockRows; j++) {
				//System.out.println(i+" "+j);
				if (!(i == numberPosition[0] && j == numberPosition[1]) && number == sudokuTable[i][j]) {
					return false;
				}
			}
		}
		
		return true;
	}
	/**
	 * @param x
	 * @param y
	 * @return sudoku element in position(x,y), if x or y invalid returns -1
	 */
	public int getSudokuElement(int x, int y)
	{
		if((x<n)&&(y<n))
			return sudokuTable[x][y];
		else
			return -1;
	}
	
	/**
	 * @param x
	 * @param y
	 */
	public void setSudokuElement(int value,int x, int y)
	{
		if((x<n)&&(y<n))
			sudokuTable[x][y] = value;
	}
		
	/**
	 * @return the sudokuTable
	 */
	public int[][] getSudokuTable() {
		return sudokuTable;
	}

	/**
	 * @param sudokuTable the sudokuTable to set
	 */
	public void setSudokuTable(int[][] sudokuTable) {
		this.sudokuTable = sudokuTable;
	}

	/**
	 * @return the p
	 */
	public int getP() {
		return p;
	}

	/**
	 * @return the q
	 */
	public int getQ() {
		return q;
	}

	/**
	 * @return the n
	 */
	public int getN() {
		return n;
	}
	
	/**
	 * @return the m
	 */
	public int getM() {
		return m;
	}
	
	/**
	 * set m
	 */
	public void setM(int m) {
		this.m = m;
	}
}

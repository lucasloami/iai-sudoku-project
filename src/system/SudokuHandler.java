package system;

import utils.Utils;

/**
 * Sudoku solver. This class has the sudoku's solutions AIs. 
 * 
 * @author rafaelsimao
 *
 */
public class SudokuHandler {

	public static final int EMPTY_VALUE = 0; 
	
	public static final int CSP_NO = 0x00000000; 
	public static final int CSP_FC = 0x00010000;
	
	public static final long TIME_OUT_PARAMETER = 60000;
	private boolean TIME_OUT;
	private int ASSIGMENTS;
	
	private int numberBlockColumns; //number of columns of each block
	private int numberBlockRows; //number of columns of each row
	private int n;
	//private int[] cell;
	//private int[] domainValues;
	
	private int[][][] sudokuDomains;
	
	public SudokuHandler() {
		// TODO Auto-generated constructor stub
	}
	
	public SudokuHandler(int p, int q, int n) {
		// TODO Auto-generated constructor stub
		this.numberBlockColumns = n/p;
		this.numberBlockRows = n/q;
		this.n = n;
		this.sudokuDomains = null;
	}
	
	/**
	 * 
	 * Execute the AllDiff Heuristic
	 * 
	 * @param number
	 * @param numberPosition
	 * @param sudokuTable
	 * @return
	 */
	public boolean applyAllDiff(int number, int[] numberPosition, int[][] sudokuTable) {
		//boolean flag = true;
		
		//check AllDiff - row
		for (int i = 0; i < n; i++) {
			if ((i != numberPosition[1]) && (number == sudokuTable[numberPosition[0]][i])) {
				return false;
			}
		}
		
		//check AllDiff - column
		for (int i = 0; i < n; i++) {
			if ((i != numberPosition[0]) && (number == sudokuTable[i][numberPosition[1]])) {
				return false;
			}
		}
		//Block Checking
		int blockX = (int)(Math.floor(numberPosition[0]/numberBlockRows)*numberBlockRows);
		int blockY = (int)(Math.floor(numberPosition[1]/numberBlockColumns)*numberBlockColumns);
		for (int i = blockX; i < blockX + numberBlockColumns; i++) {
			for (int j = blockY; j < blockY + numberBlockRows; j++) {
				if (!(i == numberPosition[0] && j == numberPosition[1]) && number == sudokuTable[i][j]) {
					return false;
				}
			}
		}
		
		return true;
	}
	
	
	/**
	 * Solve sudoku problem
	 * 
	 * @param sudokuTable
	 * @return
	 */
	public int[][] solveSudoku(int[][] sudokuTable,int csp) {
		//int assignRemaining = Utils.countIf(sudokuTable, this.n);
		
		prepareToSolve(sudokuTable,csp);
		sudokuTable= backtrack(sudokuTable,csp);
		
		/*
		if(sudokuTable == null)
			System.out.println("Nao Resolveu!");
		else
			System.out.println("Resolveu!");
		
		System.out.println(TIME_OUT);
		System.out.println(ASSIGMENTS);
		*/
	
		return sudokuTable;
	}
	
	private void prepareToSolve(int[][] sudokuTable, int csp){
		TIME_OUT = false;
		ASSIGMENTS = 0;
		
		if((csp&CSP_FC)!= 0)//{
			initSudokuDomains(sudokuTable);
			//System.out.println("COM FC");
		//}else
			//System.out.println("SEM FC");
	}
	
	
	/**
	 * 
	 * Executes backtracking in order do solve sudoku
	 * 
	 * @param assignment
	 * @return
	 */
	private int[][] backtrack(int[][] assignment, int csp) {
		if (Utils.countIf(assignment, 0) == 0) {
			return assignment;
		}
		
		int[][] result = null;
		
		//cell = variable position and current assignment
		int[] cell = selectUnassignedVariable(assignment, csp);
		//cell possible domain values
		int[] domainValues = orderDomainValues(cell, assignment, csp);
		//cell position
		int[] numberPosition = {cell[1], cell[2]};
		
		
		
		for (int i = 0; i < domainValues.length; i++) {
			if(System.currentTimeMillis() - Facade.getInstance().getTimeTotal() > TIME_OUT_PARAMETER ){
				TIME_OUT = true;
				return null;
			}
			//if value is consistent with assignment
			//if((domainValues[i]!=EMPTY_VALUE)&&
			//		(applyAllDiff(domainValues[i], numberPosition, assignment))) {
			if(isConsistent(domainValues[i], numberPosition, assignment, csp)){
				//add{var = value} to assignment
				ASSIGMENTS++;
				cell[0] = domainValues[i];
				assignment[cell[1]][cell[2]] = cell[0];
				if((csp&CSP_FC)!=0)
					applyForwardChecking(cell, true);
				
				//inferences is an matrix in which each row describes 
				//the value, row and columns of a cell in the sudoKuTable
				int[][] inferences = inference(assignment, cell, csp);
				if (inferences != null) {
					//add inferences to assignment
					assignment = Utils.fillArray(assignment, inferences);
					//result <- backtrack()
					result = backtrack(assignment,csp);
					//if result different from fail
					if (result != null) {
						return result;
					}
				}
				//System.out.println("payback "+cell[0]);
				assignment[cell[1]][cell[2]] = 0;
				if((csp&CSP_FC)!=0)
					applyForwardChecking(cell, false);
			}

		}
		return null;
	}
	
	/**
	 * Verify if the assignment is consistent based in the csp
	 * @param value
	 * @param numberPosition
	 * @param assignment
	 * @param csp
	 * @return
	 */
	private boolean isConsistent(int value, int[] numberPosition, int[][] assignment, int csp){
		boolean result = false;
		
		//If it is using Foward Checking 
		if((csp&CSP_FC)!=0){
			if((value!=EMPTY_VALUE)&&(applyAllDiff(value, numberPosition, assignment))){
				result = true;
				//if (!applyAllDiff(value, numberPosition, assignment))
				//	if(numberPosition[0]==1)
				//		System.out.println("v "+value+" i,j "+numberPosition[0]+","+numberPosition[1]);
			}
		}
		//If it does not use anything == CSP_NO
		else{
			if (applyAllDiff(value, numberPosition, assignment))
				result = true;
		}
		
		//Return if it is or not consistent
		return result;
	}

	/**
	 * 
	 * Select a variable to be assigned according to a 
	 * heuristic
	 * 
	 * @param assignment
	 * @return
	 */
	private int[] selectUnassignedVariable(int[][] assignment, int csp) {
		int k[] = new int[3];
		// TODO Implement heuristics
		// Suggestion = do a switch-case and redirect to a specific
		// method related to a heuristic
		
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (assignment[i][j] == 0) {
					k[0] = 0; //value
					k[1] = i; //row
					k[2] = j; //column
					//System.out.println("i="+i+" j="+j);
					return k;
				}
					
			}
		}
		return k;
	}
	
	/**
	 * Returns the domains possibilities for the current variable
	 * @param cell
	 * @param assignment
	 * @param csp
	 * @return
	 */
	private int[] orderDomainValues(int[] cell, int[][] assignment, int csp) {
		int[] array = {1,2,3,4,5,6,7,8,9};
		
		if((csp&CSP_FC) != 0){
			array = sudokuDomains[cell[1]][cell[2]];
			//System.out.println("i,j "+cell[1]+","+cell[2]+" "+array[0]+array[1]+array[2]+array[3]+array[4]+array[5]+array[6]+array[7]+array[8]);
		}
		
		return array;
	}

	/**
	 * Makes the inferences procedures
	 * @param assignment
	 * @param cell
	 * @param csp
	 * @return
	 */
	private int[][] inference(int[][] assignment, int[] cell, int csp) {
		int[][] matrix = new int[1][3];
		
		matrix[0] = cell;
		return matrix;
	}
	
	/**
    *
    * @param sudokuTable
    * @return
    */
	private void initSudokuDomains(int[][] sudokuTable) {
		sudokuDomains = new int[n][n][n];
          
		for(int i = 0; i<n; ++i){
			for (int j = 0; j < n; j++) {
				for (int k = 0; k < n; k++) {
					sudokuDomains[i][j][k] = k+1;
				}
			}
		}
          
		int[] cell = new int[3];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if(sudokuTable[i][j]!=EMPTY_VALUE) {
					cell[0] = sudokuTable[i][j];
					cell[1] = i;
					cell[2] = j;
					applyForwardChecking(cell,true);
				}
			}
		}
          
   }
  
   /**
    *
    * @param assignment
    * @return
    */
	/*
	private int[][] assignmentToSudoku(int[][][] assignment){
          
		int[][] sudoku = new int[n][n];
          
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				sudoku[i][j] = assignment[i][j][0];
			}
		}
          
		return sudoku;
	}
	*/
	
	/**
	 * Save and apply foward checking
	 * @param cell
	 * @param isDecreaseDomain
	 */
	private void applyForwardChecking(int[] cell, boolean isDecreaseDomain) {
		//boolean flag = true;
		
		//apply FC - row
		for (int i = 0; i < n; i++) {
			if (i != cell[2]) 
				searchAndApplyFC(cell[0], this.sudokuDomains[cell[1]][i], isDecreaseDomain);
		}
		
		//apply FC - column
		for (int i = 0; i < n; i++) {
			if (i != cell[1])
				searchAndApplyFC(cell[0], this.sudokuDomains[i][cell[2]], isDecreaseDomain);
		}
		
		//Block FC
		int blockX = (int)(Math.floor(cell[1]/numberBlockRows)*numberBlockRows);
		int blockY = (int)(Math.floor(cell[2]/numberBlockColumns)*numberBlockColumns);
		for (int i = blockX; i < blockX + numberBlockColumns; i++) {
			for (int j = blockY; j < blockY + numberBlockRows; j++) {
				if (i != cell[1] && j != cell[2])
					searchAndApplyFC(cell[0], this.sudokuDomains[i][j], isDecreaseDomain);
			}
		}
	}

	private void searchAndApplyFC(int number, int[] domain, boolean isDecreaseDomain) {
		if (isDecreaseDomain) 
			domain[number-1] = EMPTY_VALUE;
		else
			domain[number-1] = number;
	}
	
	/**
	 * get if is Time out
	 * @return
	 */
	public boolean isTimeOut(){
		return TIME_OUT;
	}

	/**
	 * @return the Assignments
	 */
	public int getAssignments() {
		return ASSIGMENTS;
	}
	
}
